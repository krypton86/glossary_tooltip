<?php

namespace Drupal\glossary_tooltip\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\Entity\Term;

class GlossaryManager {


  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $termStorage;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  protected string $cacheId = 'glossary_terms_array';

  protected array $termList;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, CacheBackendInterface $cache) {
    $this->entityTypeManager = $entityTypeManager;
    $this->cache = $cache;
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * @param $text
   *   Original string.
   *
   * @return string
   *   String with highlighted terms.
   */
  public function replaceFieldValue($text, $vocabulary = 'glossary') {
    $this->termList = $this->getTerms($vocabulary);
    $new_value = '';
    if (!empty($text)) {
      $new_value = $text;
      foreach ($this->termList as $term_id => $term_array) {
        $name = preg_quote($term_array['name'], '/');
        preg_match_all("/$name+/i", $new_value, $matches);
        if (is_array($matches[0]) && count($matches[0]) >= 1) {
          $matches = array_unique($matches[0]);
          foreach ($matches as $match) {
            $new_value = str_replace($match, '<span class="glos-term" data-gterm="' . $term_id . '">'.$match.'</span>', $new_value);
          }
        }
      }
    }

    return $new_value;
  }

  /**
   * Get an updated list of glossary terms.
   *
   * @return array
   *   Glossary term list
   */
  private function getTerms($vocabulary) {
    $cacheId = $this->cacheId . '-' . $vocabulary;
    $data = $this->cache->get($cacheId);
    if (empty($data)) {
      $terms = $this->updateTermList($vocabulary);
    }
    else {
      $query = $this->termStorage->getQuery();
      $query->condition('vid', $vocabulary);
      $query->condition('status', true);
      $query->accessCheck();
      $entity_ids = $query->execute();
      // Someone could have added new term since last cache.
      if (count($entity_ids) == count($data->data)) {
        // Get the list from cache.
        $terms = $data->data;
      }
      else {
        // Set cache again.
        $terms = $this->updateTermList($vocabulary);
      }

    }
    return $terms;
  }

  /**
   * Update term list.
   *
   * @param $vocabulary
   *   Glossary vocabulary id.
   *
   * @return array
   *   Glossary term list
   */
  private function updateTermList($vocabulary) {
    $terms = $this->termStorage->loadByProperties(['vid' => $vocabulary,'status'=> true]);
    $terms_array = [];
    /**
     * @var  integer $tid
     * @var Term $term
     */
    foreach ($terms as $tid => $term) {
      $terms_array[$tid] = [
        'tid' => $tid,
        'name' => $term->getName(),
      ];
    }
    $cacheId = $this->cacheId . '-' . $vocabulary;
    $this->cache->set($cacheId, $terms_array, CacheBackendInterface::CACHE_PERMANENT, [$vocabulary]);
    return $terms_array;
  }
}
