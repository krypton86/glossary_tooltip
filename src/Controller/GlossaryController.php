<?php

namespace Drupal\glossary_tooltip\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class GlossaryController.
 */
class GlossaryController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entity_repository;

  /**
   * Constructs a new GlossaryController object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityRepositoryInterface $entity_repository
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entity_repository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.repository')
    );
  }


  /**
   * Helper to get term by id.
   */
  public function apiGetTermById($tid) {
    $results = ['message' => 'No results found for ' . $tid];
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $term = $term_storage->load($tid);
    if (!empty($term)) {
      $term = $this->entity_repository->getTranslationFromContext($term);
      $results = [
        'name' => $term->getName(),
        'tid' => $term->id(),
        'description' => $term->getDescription(),
      ];
    }
    \Drupal::moduleHandler()->invokeAll('term_glossary_alter_result', [
      &$results,
      $term,
      $tid,
    ]);
    return new JsonResponse($results, 200);
  }

}
