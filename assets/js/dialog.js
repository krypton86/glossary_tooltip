
/**
 * @file
 * Js for Glossary Dialog .
 */

(function ($, Drupal, once) {
  'use strict';
  Drupal.behaviors.GlossaryContent = {
    attach: function (context) {
      if (context === document) {
        $('body').append('<div id="glossary-dialog" class="hidden" title="Glossary:"><div id="glossary-dialog-inner"><div class="ajax-progress ajax-progress-throbber"><div class="ajax-throbber">' + Drupal.t('Loading...') + '</div></div></div></div>');
      }
      var $dialog = $(once('glossary-dialog', '#glossary-dialog'));
      $(once('glossaryListenner', '.glos-term')).on('click', function (event) {
        event.preventDefault();
        var termId = $(this).data('gterm');
        var termText = $(this).text();
        $dialog.dialog({
          autoOpen: true,
          resizable: false,
          modal: true,
          close: function () {
            $(this).find('#glossary-dialog-inner').html('<div class="ajax-progress ajax-progress-throbber"><div class="ajax-throbber">' + Drupal.t('Loading...') + '</div></div>');
          },
        });
        // Call Ajax here.
        var url = Drupal.url('glossary-get-term-by-id/' + termId);
        $.ajax({
          url: url,
          dataType: "json",
          type: "GET",
          success: function (data) {
            var $dialogInner = $dialog.find('#glossary-dialog-inner');
            $dialogInner.html('');
            if (data.name && data.description) {
              $dialogInner.append('<b>' + data.name + '</b><br/><br/>');
              var description = data.description;
              if (description.length > 100) {
                description = description.substring(0, 100) + '...';
                $dialogInner.append(description);
                $dialogInner.append('<br/><button id="read-more-button">Read more</button>');

                $('#read-more-button').on('click', function () {
                  $dialogInner.html('<b>' + data.name + '</b><br/>' + data.description);
                  $(this).remove();
                });
              } else {
                $dialogInner.append(description);
              }

              $dialogInner.removeClass('hidden');
              var event = new CustomEvent('glossary_modal_show', {detail: data});
              document.dispatchEvent(event);
            }
            else {
              $dialogInner.html('No results found for' + termText);
            }
          }
        });

      });

    }
  };
})(jQuery, Drupal, once);
