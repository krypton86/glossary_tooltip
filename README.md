# Glossary tooltip module

Installation:

1. Download module and enable it with `drush en glossary_tooltip -y` or by using UI.
2. Check if Glossary vocabulary was created: `/admin/structure/taxonomy/manage/glossary/overview`
3. Taxonomy term glossary will parse the entity text fields rendered string to add html tags around found words. Go to the display options of the text fields you want to parse in the entity Manage display form and check the option "Enable glossary".
